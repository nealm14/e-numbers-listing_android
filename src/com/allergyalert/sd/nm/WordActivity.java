/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.allergyalert.sd.nm;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Displays a word and its definition.
 */
public class WordActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word);

        Uri uri = getIntent().getData();
        final Cursor cursor = managedQuery(uri, null, null, null, null);

        if (cursor == null) {
            finish();
        } else {
            cursor.moveToFirst();

            TextView word = (TextView) findViewById(R.id.word);
            TextView definition = (TextView) findViewById(R.id.definition);

            final int wIndex = cursor.getColumnIndexOrThrow(DictionaryDatabase.KEY_WORD);
            int dIndex = cursor.getColumnIndexOrThrow(DictionaryDatabase.KEY_DEFINITION);

            word.setText(cursor.getString(wIndex));
            definition.setText(cursor.getString(dIndex));
            
            Button googleButton = (Button) findViewById(R.id.googleQuery);
            googleButton.setOnClickListener(new View.OnClickListener() {
            	public void onClick(View view) {
            		Intent numberSearch = new Intent(Intent.ACTION_VIEW, 
    						Uri.parse("http://www.google.com/search?q=" + cursor.getString(wIndex)));
    				startActivity(numberSearch);;
            	}
        			}); 
        }
        
        
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                onSearchRequested();
                return true;
            case R.id.shareMenu: 
        		Intent i = new Intent (Intent.ACTION_SEND);
        	   	i.setType("text/plain");
        	   	i.putExtra(Intent.EXTRA_SUBJECT, "I've been using Allergy Alert");
        	   	i.putExtra(Intent.EXTRA_TEXT, "E Numbers listing and Allergy alert app android app. Download it from the Android Market https://market.android.com/details?id=com.allergyalert.sd.nm");
        	   	Intent chooser = Intent.createChooser(i, "Share Allergy Alert");
        	   	startActivity(chooser);
            default:
                return false;
        }
    }
}
