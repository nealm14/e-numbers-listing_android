package com.allergyalert.sd.nm;

import java.util.ArrayList;
import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AntioxidantsListing extends ListActivity{

	public static int sendPosition;
	int apiVersion = android.os.Build.VERSION.SDK_INT;
	
	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.colourlistview); 
		if (apiVersion != 8) { //api version is the android build v8 = android 2.2 9& is gBread , v14 & 15 is ICS 
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder() //this strict mode stuff is needed for the network on gingerbread & higher
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		ArrayList<HashMap<String, String>> calendar = new ArrayList<HashMap<String, String>>();
		
		String xml = ParserEngine.getXML();
		Document doc = ParserEngine.XMLfromString(xml);
		
		int numEvents = ParserEngine.numEvents(doc);
		
		if ((numEvents <= 0)){
			Toast.makeText(AntioxidantsListing.this, "Error, No Listings Found", Toast.LENGTH_LONG).show();
			finish();
		}
		
		NodeList nodes = doc.getElementsByTagName("antioxidant");
		
		for (int i = 0; i < nodes.getLength(); i++) {
			HashMap<String, String> map = new HashMap<String, String>();
			
			Element e = (Element)nodes.item(i);
			map.put("id", ParserEngine.getValue(e, "id"));
			map.put("number", ParserEngine.getValue(e, "number"));
			map.put("name", ParserEngine.getValue(e, "name"));
			map.put("link", ParserEngine.getValue(e,  "link")); // full wiki url
			map.put("validlink", ParserEngine.getValue(e,  "validlink")); // 0 for no link , 1 for yes 
		    calendar.add(map);
		}
		
	   ListAdapter adapter = new SimpleAdapter(this, calendar, R.layout.colourlistviewtext, 
			   new String[] { "number", "name"},
			   new int[] { R.id.item_title, R.id.item_subtitle });
	   
	   setListAdapter(adapter);
	   
	   final ListView lv = getListView();
	   lv.setTextFilterEnabled(true);
	   lv.setOnItemClickListener(new OnItemClickListener() {
		   public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				@SuppressWarnings("unchecked")
			    HashMap<String, String> o = (HashMap<String, String>) lv.getItemAtPosition(position);	        		
				
				
				sendPosition = position;
				
				String link = o.get("validlink");
				int linkSwitch = new Integer(link);
				
				switch (linkSwitch)
				{
				case 0:
					Toast.makeText(AntioxidantsListing.this, "Searching details for Antioxidant - " + o.get("number"), Toast.LENGTH_LONG).show(); 
					Intent openSearch = new Intent(Intent.ACTION_VIEW, 
							Uri.parse("http://www.google.com/search?q=" + o.get("name")));
					startActivity(openSearch);
					break;
				case 1:
					Toast.makeText(AntioxidantsListing.this, "Opening details for Antioxidant - " + o.get("number"), Toast.LENGTH_LONG).show(); 
					Intent openWeb = new Intent(Intent.ACTION_VIEW, 
							Uri.parse(o.get("link")));
					startActivity(openWeb);
					break;
				}
		   }				 
		   
				
	   });
		
	}
	
	
	
}