package com.allergyalert.sd.nm;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



public class ParserEngine {
	
	public static final Document XMLfromString(String xml){
		
		Document doc = null;
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	
		try {
				DocumentBuilder db = dbf.newDocumentBuilder();
				
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(xml));
				doc = db.parse(is);
				
		} catch (ParserConfigurationException e) {
			System.out.println("XML parse error: " + e.getMessage());
		
	
	    } catch (SAXException e) {
	    	System.out.println("Wrong XML file Structure: " + e.getMessage());
		return null;
		
	    } catch (IOException e ) {
	    	System.out.println("I/O exception: " + e.getMessage());
	    	return null;
	    }
		
		return doc;
	}

	 public final static String getElementValue( Node elem ) {
	     Node kid;
	     if( elem != null){
	         if (elem.hasChildNodes()){
	             for( kid = elem.getFirstChild(); kid != null; kid = kid.getNextSibling() ){
	                 if( kid.getNodeType() == Node.TEXT_NODE  ){
	                     return kid.getNodeValue();
	                 }
	             }
	         }
	     }
	     return "";
	 }
		 
	 public static String getXML(){	 
			String line = null;

			try {	
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost("http://nealmarriott.com/app-xml/enumbers/listing.xml");

				HttpResponse httpResponse = httpClient.execute(httpPost);
				HttpEntity httpEntity = httpResponse.getEntity();
				line = EntityUtils.toString(httpEntity);
			} catch (UnsupportedEncodingException e) {
				line = "<additives status=\"error\"><msg>Can't connect to server</msg></additives>";
			} catch (MalformedURLException e) {
				line = "<additives status=\"error\"><msg>Can't connect to server</msg><additives>";
			} catch (IOException e) {
				line = "<additives status=\"error\"><msg>Can't connect to server</msg></additives>";
			}

			return line; 

	}
	 
	public static int numEvents(Document doc){		
		Node events = doc.getDocumentElement();
		int res = -1;
		
		try{
			res = Integer.valueOf(events.getAttributes().getNamedItem("count").getNodeValue());
		}catch(Exception e ){
			res = -1;
		}
		
		return res;
	}

	public static String getValue(Element item, String str) {		
		NodeList n = item.getElementsByTagName(str);		
		return ParserEngine.getElementValue(n.item(0));
	}
}