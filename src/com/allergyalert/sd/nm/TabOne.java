package com.allergyalert.sd.nm;

import com.google.ads.*;
import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;


public class TabOne extends Activity {
    
	  private TextView mTextView;
	  private ListView mListView;
	  private AdView adView;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_one);
        
        AdView adView = (AdView)this.findViewById(R.id.adView);
        adView.loadAd(new AdRequest());
        
       
      
    Button coloursButton = (Button) findViewById(R.id.ColoursButton);
    coloursButton.setOnClickListener(new View.OnClickListener() {
    	public void onClick(View view) {
 		startActivity(new Intent (TabOne.this, ColoursListing.class));
    	}
			}); 
    
    Button preserveButton = (Button) findViewById(R.id.PreserveButton);
    preserveButton.setOnClickListener(new View.OnClickListener() {
    	public void onClick(View view) {
 		startActivity(new Intent (TabOne.this, PreservativesListing.class));
    	}
			}); 
    
    Button antioxidantsButton = (Button) findViewById(R.id.AntioxidantsButton);
    antioxidantsButton.setOnClickListener(new View.OnClickListener() {
    	public void onClick(View view) {
 		startActivity(new Intent (TabOne.this, AntioxidantsListing.class));
    	}
			}); 
    
    Button sweetenerButton = (Button) findViewById(R.id.SweetenersButton);
    sweetenerButton.setOnClickListener(new View.OnClickListener() {
    	public void onClick(View view) {
 		startActivity(new Intent (TabOne.this, SweetenersListing.class));
    	}
			}); 
    
    Button emulsifierButton = (Button) findViewById(R.id.EmulsifierButton);
    emulsifierButton.setOnClickListener(new View.OnClickListener() {
    	public void onClick(View view) {
 		startActivity(new Intent (TabOne.this, EmulsifiersListing.class));
    	}
			}); 
    
    Button miscButton = (Button) findViewById(R.id.MiscButton);
    miscButton.setOnClickListener(new View.OnClickListener() {
    	public void onClick(View view) {
 		startActivity(new Intent (TabOne.this, MiscListing.class));
    	}
			}); 
    
    Button searchButton = (Button) findViewById(R.id.searchActivityButton);
    searchButton.setOnClickListener(new View.OnClickListener() {
    	public void onClick(View view) {
 		startActivity(new Intent (TabOne.this, TabTwo.class));
    	}
			}); 
  
    
}
    
    
}



/*Button testingButton = (Button) findViewById(R.id.searchButton);
testingButton.setOnClickListener(new View.OnClickListener() {
	public void onClick(View view) {
		startActivity(new Intent (TabOne.this, TestClass.class));

		}); */



/* final EditText searchQuery = (EditText) findViewById(R.id.searchBox);
searchQuery.setOnKeyListener(new OnKeyListener(){
	public boolean onKey(View v, int keyCode, KeyEvent event) {
	if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
            (keyCode == KeyEvent.KEYCODE_ENTER)) {
          // Perform action on key press

	

	    
          //Toast.makeText(TabOne.this, searchQuery.getText(), Toast.LENGTH_SHORT).show();
          return true;
        }
       return false;
    }

});	*/
