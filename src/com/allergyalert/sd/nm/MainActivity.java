package com.allergyalert.sd.nm;

import com.google.ads.*;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabHost;


public class MainActivity extends TabActivity {

	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final TabHost tabHost = getTabHost();

        tabHost.addTab(tabHost.newTabSpec("tab1")
                .setIndicator("E Number Listings")
                .setContent(new Intent(this, TabOne.class)));
  
        
        	tabHost.addTab(tabHost.newTabSpec("tab2")
                .setIndicator("UK FSA allergy recall news")
                .setContent(new Intent(this, TabThree.class)));
        
    }
    
    @Override 
    public boolean onCreateOptionsMenu(Menu menu1) {
    super.onCreateOptionsMenu(menu1);
    MenuInflater mi = getMenuInflater();
  	 mi.inflate(R.menu.tab_menu, menu1);
  	return true;
    } 
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item1) {
    	switch (item1.getItemId()) {
    	
    	case R.id.tab_menu1: 
    		Intent i = new Intent (Intent.ACTION_SEND);
    	   	i.setType("text/plain");
    	   	i.putExtra(Intent.EXTRA_SUBJECT, "I've been using Allergy Alert");
    	   	i.putExtra(Intent.EXTRA_TEXT, "E Numbers listing and Allergy alert android app. Download it from Google Play https://play.google.com/store/apps/details?id=com.allergyalert.sd.nm");
    	   	Intent chooser = Intent.createChooser(i, "Share Allergy Alert");
    	   	startActivity(chooser);
    	break;
    
}
		return true;
    	
}
 
}